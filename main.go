package main

import (
	"log"

	"stress/config"
	"stress/highload"
	"stress/tests"
)

func main() {
	config.InitTestCfg()
	highload.InitTestClient()

	routes := []highload.RouteTest{
		tests.GetTestDataUserLogin(config.TestCfg),
		// tests.GetTestDataUserReg(config.TestCfg),
		tests.GetTestDataItemCount(config.TestCfg),
		tests.GetTestDataOrdersGetAll(config.TestCfg),
		tests.GetTestDataItemNew(config.TestCfg),
	}

	if err := highload.NewHTTPStressTest(routes); err != nil {
		log.Fatalf("create HTTP stress test fail: %s", err)
	}
}
