package tests

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"

	"stress/config"
	"stress/highload"
)

const (
	apiItemCount = "/item/getcount"
	apiItemNew   = "/item/newitem"
)

func GetTestDataItemCount(cfg config.TestCfgT) highload.RouteTest {
	header := http.Header{}
	header.Set("Authorization", token)
	return highload.NewRouteTest(
		cfg.RPM, cfg.RPMPerRoutine,
		defaultScheme, cfg.AddrPort.String(), http.MethodGet, apiItemCount,
		nil, header,
	)
}

func GetTestDataItemNew(cfg config.TestCfgT) highload.RouteTest {
	header := http.Header{}
	header.Set("Authorization", token)

	genBodyFn := func() io.Reader {
		testData := struct {
			Name      string `json:"name"`
			RestCount int    `json:"restCount"`
			Price     int    `json:"price"`
		}{
			Name:      "order2123",
			RestCount: 123,
			Price:     123123421,
		}

		res, err := json.Marshal(testData)
		if err != nil {
			log.Printf("fake data gen fail: %s", err)
			return nil
		}

		return bytes.NewReader(res)
	}

	return highload.NewRouteTest(
		cfg.RPM, cfg.RPMPerRoutine,
		defaultScheme, cfg.AddrPort.String(), http.MethodPost, apiItemNew,
		genBodyFn, header,
	)
}
