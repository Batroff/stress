package tests

import (
	"net/http"

	"stress/config"
	"stress/highload"
)

const (
	apiOrderGetAll = "/order/getall"
)

func GetTestDataOrdersGetAll(cfg config.TestCfgT) highload.RouteTest {
	header := http.Header{}
	header.Set("Authorization", token)
	return highload.NewRouteTest(
		cfg.RPM, cfg.RPMPerRoutine,
		defaultScheme, cfg.AddrPort.String(), http.MethodGet, apiOrderGetAll+"?id="+id,
		nil, header,
	)
}
