package tests

import (
	"bytes"
	"encoding/json"
	"io"
	"log"
	"net/http"

	"github.com/jaswdr/faker"

	"stress/config"
	"stress/highload"
)

const (
	defaultScheme = "http"

	token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibG9naW4iOiJhZG1pbiIsImVtYWlsIjoiYWRtaW5AbXkuZG9tYWluIiwiaWF0IjoxNjgwNjMzNDM1LCJleHAiOjE2ODA3MTk4MzV9.WcbCv15DzOjKqvnR8eRhdT83-j3dCE4ZQc-pmacA5EY"
	id    = "1"

	apiUserReg   = "/user/registration"
	apiUserLogin = "/user/login"
)

func GetTestDataUserReg(cfg config.TestCfgT) highload.RouteTest {
	email := faker.New().Person().Faker.Internet().Email()
	genBodyFn := func() io.Reader {
		testData := struct {
			Login    string `json:"login"`
			Email    string `json:"email"`
			Password string `json:"password"`
		}{
			Login:    email,
			Email:    email,
			Password: "password",
		}

		res, err := json.Marshal(testData)
		if err != nil {
			log.Printf("fake data gen fail: %s", err)
			return nil
		}

		return bytes.NewReader(res)
	}

	header := http.Header{}
	header.Set("Content-Type", "application/json")
	return highload.NewRouteTest(
		cfg.RPM, cfg.RPMPerRoutine,
		defaultScheme, cfg.AddrPort.String(), http.MethodPost, apiUserReg,
		genBodyFn, header,
	)
}

func GetTestDataUserLogin(cfg config.TestCfgT) highload.RouteTest {
	genBodyFn := func() io.Reader {
		testData := struct {
			Email    string `json:"email"`
			Password string `json:"password"`
		}{
			Email:    "test@my.test",
			Password: "secure",
		}

		res, err := json.Marshal(testData)
		if err != nil {
			log.Printf("fake data gen fail: %s", err)
			return nil
		}

		return bytes.NewReader(res)
	}

	header := http.Header{}
	header.Set("Content-Type", "application/json")
	return highload.NewRouteTest(
		cfg.RPM, cfg.RPMPerRoutine,
		defaultScheme, cfg.AddrPort.String(), http.MethodPost, apiUserLogin,
		genBodyFn, header,
	)
}
