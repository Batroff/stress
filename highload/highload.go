package highload

import (
	"context"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"time"
)

var (
	ErrEmptyRouteLimits = errors.New("empty route limits map")
	ErrUnexpected       = errors.New("unexpected fail")

	defaultClient *http.Client
)

const (
	routineChanSize = 4
)

func InitTestClient() {
	defaultClient = &http.Client{Transport: &http.Transport{ResponseHeaderTimeout: time.Second * 3}}
	initLogger()
}

type httpTestData struct {
	reqFail    bool
	reqFailErr error

	httpRes *http.Response

	tmFull time.Duration
}

type httpTestRes struct {
	statusSpread map[string]int
	errList      []error

	tmgAvg time.Duration
	tmgMax time.Duration
	tmgMin time.Duration
}

func (t httpTestRes) String() string {
	return fmt.Sprintf(
		"%v, errCnt=%d, timings=(min=%s, max=%s, avg=%s)",
		t.statusSpread, len(t.errList), t.tmgMin, t.tmgMax, t.tmgAvg,
	)
}

func newHTTPTestRes(data []httpTestData) httpTestRes {
	errList := make([]error, 0)
	statusSpread := make(map[string]int, len(data))
	var (
		timingAvg, timingMax, timingMin time.Duration
		timingSum, timingN              int64
	)

	for _, item := range data {
		if item.reqFail {
			errList = append(errList, item.reqFailErr)
			continue
		}

		if _, ok := statusSpread[item.httpRes.Status]; !ok {
			statusSpread[item.httpRes.Status] = 0
		}

		statusSpread[item.httpRes.Status] += 1

		tmFullNano := item.tmFull.Nanoseconds()

		// Avg timing
		timingSum += tmFullNano
		timingN++

		if tmFullNano > timingMax.Nanoseconds() {
			timingMax = time.Duration(tmFullNano)

			// Init timingMin
			if timingMin == 0 {
				timingMin = timingMax
			}
		}

		if tmFullNano < timingMin.Nanoseconds() {
			timingMin = time.Duration(tmFullNano)
		}
	}

	if timingN > 0 {
		timingAvg = time.Duration(timingSum / timingN)
	}

	return httpTestRes{
		statusSpread: statusSpread,
		errList:      errList,
		tmgAvg:       timingAvg,
		tmgMax:       timingMax,
		tmgMin:       timingMin,
	}
}

type RouteTest struct {
	lg *testLogger

	rpm               int
	goroutineInterval time.Duration
	goroutineNum      int

	scheme string
	addr   string
	method string
	path   string
	bodyFn func() io.Reader
	header http.Header

	wg   sync.WaitGroup
	done chan bool

	mtx sync.RWMutex
	res []httpTestData
}

func NewRouteTest(
	rpm, rpmPerRoutine int,
	scheme, addr, method, path string,
	bodyFn func() io.Reader, headers http.Header,
) RouteTest {
	logName := strings.NewReplacer("/", "_", "?", "_").Replace(path) + ".log"
	lg, err := newTestLogger(logName)
	if err != nil {
		log.Fatalf("test logger init: %s", err)
	}

	return RouteTest{
		lg: lg,

		rpm:               rpm,
		goroutineNum:      int(math.Ceil(float64(rpm / rpmPerRoutine))),
		goroutineInterval: time.Duration(time.Minute.Nanoseconds() / int64(rpmPerRoutine)),

		scheme: scheme,
		addr:   addr,
		method: method,
		path:   path,
		bodyFn: bodyFn,
		header: headers,

		done: make(chan bool),
		res:  make([]httpTestData, 0, rpm),

		mtx: sync.RWMutex{},
		wg:  sync.WaitGroup{},
	}
}

func (r *RouteTest) Scheme() string {
	if r.scheme == "" {
		return "http://"
	}
	return r.scheme
}

func (r *RouteTest) Path() string {
	if r.path == "" {
		return "/"
	}
	return r.path
}

func (r *RouteTest) Method() string {
	if r.method == "" {
		return http.MethodGet
	}
	return r.method
}

func (r *RouteTest) pushRes(result httpTestData) {
	r.mtx.Lock()
	defer r.mtx.Unlock()
	r.res = append(r.res, result)
}

func (r *RouteTest) getTestData() []httpTestData {
	r.mtx.RLock()
	defer r.mtx.RUnlock()
	return r.res
}

func (r *RouteTest) sendHTTP() {
	r.wg.Add(1)

	testRes := httpTestData{reqFail: true, reqFailErr: ErrUnexpected}
	defer func() {
		if testRes.reqFail {
			r.lg.Printf("%v||\n", testRes.reqFail)
			return
		}
		r.lg.Printf("%v|%d|%d\n", testRes.reqFail, testRes.httpRes.StatusCode, testRes.tmFull.Microseconds())
	}()

	defer r.wg.Done()

	// Create request
	var body io.Reader
	if r.bodyFn != nil {
		body = r.bodyFn()
	}

	req, err := http.NewRequestWithContext(
		context.Background(),
		r.Method(),
		fmt.Sprintf("%s://%s%s", r.Scheme(), r.addr, r.Path()),
		body,
	)
	if err != nil {
		testRes.reqFailErr = err
		r.pushRes(testRes)
		return
	}
	req.Header = r.header

	// Do request
	timeStart := time.Now()
	resp, err := defaultClient.Do(req)
	if err != nil {
		testRes.reqFailErr = err
		r.pushRes(testRes)
		return
	}
	defer resp.Body.Close()

	// Stats
	testRes.tmFull = time.Now().Sub(timeStart)
	testRes.httpRes = resp
	testRes.reqFail = false
	testRes.reqFailErr = nil
	r.pushRes(testRes)
}

func (r *RouteTest) startTest(routineIdx int) {
	ticker := time.NewTicker(r.goroutineInterval)
	total := 0

	start := time.Now()
	for {
		select {
		case <-r.done:
			log.Printf("done %s", r.path)
			return
		case <-ticker.C:
			total += 1
			go r.sendHTTP()

			// every 5 seconds print sub info
			if time.Now().Sub(start).Round(time.Millisecond*100)%(time.Second*5) == 0 {
				subRes := r.getTestData()
				log.Printf("route routine=#%d %s: total=%d, byRoutine=%d", routineIdx, r.Path(), len(subRes), total) // TODO: success=%d, fail=%d
			}
		}
	}
}

func NewHTTPStressTest(routes []RouteTest) error {
	if len(routes) == 0 {
		return ErrEmptyRouteLimits
	}

	endSig := make(chan os.Signal)
	signal.Notify(endSig, syscall.SIGINT)

	done := make(chan bool, routineChanSize)
	start := time.Now()

	for routeIdx := range routes {
		for routineIdx := 0; routineIdx < routes[routeIdx].goroutineNum; routineIdx++ {
			log.Printf("route #%d.%d %s start", routeIdx, routineIdx, routes[routeIdx].Path())

			routes[routeIdx].done = done
			go routes[routeIdx].startTest(routineIdx)
		}
	}

	// waiting for user SIGINT interruption signal (Ctrl+C)
	<-endSig
	for i := range routes {
		i := i
		log.Printf("route #%d %s: send stop signal", i, routes[i].Path())
		done <- true
	}
	for i := range routes {
		routes[i].wg.Wait()
		routes[i].lg.Close()

		log.Printf(
			"route #%d %s %s: time=%s, stats:%s",
			i, routes[i].Method(), routes[i].Path(), time.Now().Sub(start).String(), newHTTPTestRes(routes[i].res),
		)
	}

	return nil
}
