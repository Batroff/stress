package highload

import (
	"errors"
	"fmt"
	"log"
	"os"
	"sync"
)

const (
	commonLogPath = "gostresslog/"
)

func initLogger() {
	commonLogDir, err := os.Getwd()
	if err != nil {
		log.Fatalf("getwd: %s", err)
	}

	if err = os.Mkdir(commonLogDir+"/"+commonLogPath, os.ModePerm); err != nil && !errors.Is(err, os.ErrExist) {
		log.Fatalf("mkdir: %s", err)
	}
}

type testLogger struct {
	mtx sync.Mutex
	f   *os.File
}

func newTestLogger(name string) (*testLogger, error) {
	f, err := os.Create(commonLogPath+"log"+name)
	if err != nil {
		return nil, fmt.Errorf("open file: %w", err)
	}

	return &testLogger{
		f: f,
	}, nil
}

func (t *testLogger) Printf(format string, args ...interface{}) {
	t.mtx.Lock()
	defer t.mtx.Unlock()
	_, err := t.f.WriteString(fmt.Sprintf(format, args...))
	if err != nil {
		log.Printf("write string to file %s: %s", t.f.Name(), err)
	}
}

func (t *testLogger) Close() {
	t.f.Close()
}
