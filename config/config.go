package config

import (
	"log"
	"net/netip"
	"os"
	"strconv"
)

var TestCfg TestCfgT

const (
	defaultRPM           = 600
	defaultRPMPerRoutine = 600

	envKeyHost          = "HOST"
	envKeyPort          = "PORT"
	envKeyRPM           = "RPM"
	envKeyRPMPerRoutine = "RPM_PER_ROUTINE"
	envKeyVerbose       = "VERBOSE"
)

type TestCfgT struct {
	AddrPort      netip.AddrPort
	RPM           int
	RPMPerRoutine int

	Verbose bool
}

func InitTestCfg() {
	var err error
	TestCfg.AddrPort, err = netip.ParseAddrPort(os.Getenv(envKeyHost) + ":" + os.Getenv(envKeyPort))
	if err != nil {
		log.Fatalf("Invalid addr or port '%s'. Use options '%s=<IP>' '%s'=<PORT>", err, envKeyHost, envKeyPort)
	}

	TestCfg.RPM, err = strconv.Atoi(os.Getenv(envKeyRPM))
	if err != nil {
		log.Printf("RPM set to %d by default. Use '%s=<N>'", defaultRPM, envKeyRPM)
		TestCfg.RPM = defaultRPM
	}

	TestCfg.RPMPerRoutine, err = strconv.Atoi(os.Getenv(envKeyRPMPerRoutine))
	if err != nil {
		log.Printf("RPM per routine set to %d by default. Use '%s=<N>'", defaultRPMPerRoutine, envKeyRPMPerRoutine)
		TestCfg.RPMPerRoutine = defaultRPMPerRoutine
	}

	TestCfg.Verbose = os.Getenv(envKeyVerbose) != ""
	if !TestCfg.Verbose {
		log.Printf("Verbose logging disabled by default. Use '%s=true'", envKeyVerbose)
	}
}
